import os
import sys
import sqlite3
import subprocess
import shlex
from mapbox import Geocoder

token = os.getenv('mapbox_token')

class geoClass(object):
    def __init__(self):
        self._lat = 0
        self._long = 0
        self.geocoder = Geocoder(access_token=token)

    @property
    def lat(self):
        return self._lat

    @lat.setter
    def lat(self, value):
        if value > 90 or value < -90:
            raise ValueError("Latitude value outside allowable range of -90 to 90.")
        self._lat = value

    @property
    def long(self):
        return self._long

    @long.setter
    def long(self, value):
        if value > 180 or value < -180:
            raise ValueError("Longitude value outside allowable range of -180 to 180.")
        self._long = value

    def get_latlong(self, country_name):
        """
        :param country_name: name of interested country
        :return: latitude and longtitude of that country
        """
        response = self.geocoder.forward(country_name)
        # forward geocoding: https://github.com/mapbox/mapbox-sdk-py/blob/master/docs/geocoding.md#geocoding
        first = response.geojson()['features'][0]
        (self.long, self.lat) = [int(coord) for coord in first['geometry']['coordinates']]
        return self.long, self.lat


class geoTable(geoClass):
    def __init__(self, sqlite_file):
        super(geoTable, self).__init__()
        self.sqlite_file = sqlite_file

    def create_latlong(self):
        conn = sqlite3.connect(self.sqlite_file)  # 'database.sqlite'
        c = conn.cursor()
        c.execute('SELECT * FROM Country;')
        all_rows = c.fetchall()

        # create latlong table
        c.execute(
            'CREATE TABLE if NOT EXISTS latlong (id INT PRIMARY KEY, country_id INT NOT NULL, country_name VARCHAR(255) NOT NULL, lat INT NOT NULL, long INT NOT NULL);')
        result = []
        for i, row in enumerate(all_rows):
            country_id, country_name = row
            lat, long = self.get_latlong(country_name)
            result.append((i, country_id, country_name, lat, long))

        c.executemany('INSERT INTO latlong VALUES (?,?,?,?,?)', result)
        conn.commit()

