from setuptools import setup


setup(
    name='soccer_geodata_pkg',
    version='0.0.1',
    packages=['data_retrieval','geopkg'],
    entry_points={
        'console_scripts': ['download_decompress=data_retrieval.download_decompress:main']
    },
    install_requires=['mapbox', 'numpy', 'pandas']
)
