if [[ ! -f $1/esdb.md5 ]]; then
  echo "File does not exist"
  curl https://gitlab.oit.duke.edu/bios821/european_soccer_database/raw/master/esdb.md5 > $1/esdb.md5
else
  echo "File already exists"
fi

md5_download=$(cut -b 1-32 $1/esdb.md5)
md5_zip=$(md5sum $1/soccer.zip | cut -b 1-32)

if [[ $md5_download == $md5_zip ]]; then
  if [[ -e $2/soccer.zip ]]; then
    mkdir $2/data
    mv $2/soccer.zip $3
    unzip $3/soccer.zip -d $3
  fi
else
  echo "md5sum does not match"
  exit 2
fi


# ./download_decompress.sh /Users/yiran/Desktop/BIOS821/finalproj/bio821-finalproj/data_retrieval /Users/yiran/Desktop/BIOS821/finalproj/bio821-finalproj/data_retrieval/ /Users/yiran/Desktop/BIOS821/finalproj/bio821-finalproj/data_retrieval
